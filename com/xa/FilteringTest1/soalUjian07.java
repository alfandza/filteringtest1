/* 
Input : Afrika
Output :
***a***
***k***
***i***
***r***
***f***
***A***


Input : Jeruk
Output :
**k**
**u**
**r**
**e**
**J** 
*/



package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian07 {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        soalTujuh(input);
    }

    public static void soalTujuh(Scanner input) {
        System.out.print("Masukkan kata : ");
        String masukan = input.nextLine();
        ambilHurufDiantaraSensor(masukan);
    }

    public static void ambilHurufDiantaraSensor(String in) {
        String bForPrint = "";
        String bintang = "*";
        int printB = 0;
        if (in.length() % 2 == 0) printB = in.length() / 2;
        else printB = (in.length()-1)/2;

        for (int i=0;i<printB;i++){
            bForPrint += bintang;
        }

        for (int i=0;i<in.length();i++){
            char temp = in.charAt(i);
            System.out.println(bForPrint+temp+bForPrint);
            // if (in.length() > 5) System.out.println("***"+temp+"***");
            // else System.out.println("**"+temp+"**");
        }
    }

}
