/* 
Tentukan apakah kalimat ini adalah Pangram* atau bukan
"Sphinx of black quartz, judge my vow"
"Brawny gods just flocked up to quiz and vex him"
"Check back tomorrow; I will see if the book has arrived."

*Pangram adalah kata atau kalimat yang mengandung setiap abjad alphabet,
contohnya "A quick brown fox jumps over the lazy dog"
*/

package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        soalDelapan(input);
    }

    public static void soalDelapan(Scanner input) {
        System.out.print("Masukkan kalimat untuk pengecekan Pangram : ");
        String masukan = input.nextLine().toLowerCase();
        System.out.println(pangramCheck(masukan));
    }

    public static String pangramCheck(String masukan){
        String akhir = "";
        boolean[] checker = new boolean[26];
        int index = 0;
        
        if (masukan.length()<26) {
            akhir = "Huruf kurang dari 26";
            return akhir;
        }

        for (int i=0;i<masukan.length();i++){
            char temp = masukan.charAt(i);
            if (temp >= 'a' && temp <='z'){
                index = temp - 'a';
                checker[index] = true;
            } else continue;
        }
        int truechecker=0;
        for (int i=0;i<checker.length;i++){
            if (checker[i]==true) truechecker++;
        }
        
        if (truechecker == 26) akhir = "Ini pangram";
        else akhir = "Ini bukan pangram";
        return akhir;
    }

}
