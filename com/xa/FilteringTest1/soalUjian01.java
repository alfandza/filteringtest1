/* 
Tentukan sebuah kata/angka adalah palindrome atau tidak. (Palindrome adalah kata, atau nomor, atau urutan karakter yang jika dibaca dari depan maupun belakang akan sama saja. Contohnya: "malam", "12021", "katak")

Input : Berupa string
Output : "YES"/"NO"

Contoh :
Input :    "katak"
Output : "YES"

Input :    "kelas"
Output : "NO"

Constrains :    String dibolak balik hasilnya sama
*/

package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian01 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalSatu(scan);
    }

    public static void soalSatu(Scanner scan) {
        System.out.print("Masukkan input : ");
        String input = scan.nextLine();
        String output = "";

        output = reverseString(input);

        System.out.println(input);
        System.out.println(output);
        if (output.equals(input)){
            System.out.println("YES");
        }else System.out.println("NO");
        
    }
    
    //method
    public static String reverseString(String input){
        String output = "";
        for (int i=(input.length()-1);i>=0;i--){
            char temp = input.charAt(i);
            output += temp;
        }
        return output;
    }

}