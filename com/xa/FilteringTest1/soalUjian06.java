/* 
INPUT
	                        OUTPUT
Susilo Bambang Yudhoyono
	                        S***o B***g Y***o
Rani Tiara
	                        R***i T***a
*/


package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian06 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalEnam(scan);
    }

    public static void soalEnam(Scanner scan){
        System.out.print("Masukkan input : ");
        String[] nama = scan.nextLine().split(" ");
        sensorTengahTiga(nama);

    }

    public static void sensorTengahTiga(String[] inputNama){
        for (int i=0;i< inputNama.length; i++) {
            for (int j = 0; j < inputNama[i].length(); j++) {
                char sementara = inputNama[i].charAt(j);
                if (j == 0 || j == inputNama[i].length() - 1)
                    System.out.print(sementara);
                else System.out.print("*");
                // else continue;
            }
            System.out.print(" ");
        }
        System.out.println();
    }
}
