/* 
125gr terigu, 100gr gula pasir, 100mL susu adalah resep untuk membuat 15 cupcake. 
Berapa takaran terigu, gula, dan susu untuk membuat sebanyak n cupcake?
*/


package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        soalSembilan(input);
    }

    public static void soalSembilan(Scanner input) {
        System.out.print("Masukkan berapa banyak Cupcakes yang akan dibuat : ");
        int masukan = input.nextInt();
        double[] total = prosesCupcake(masukan);
        System.out.println("Totalnya adalah "+total[0]+" gram terigu dan "+total[1]+" gram gula dan "+total[2]+" mL susu");
    }

    public static double[] prosesCupcake(int banyak){
        double[] totalan = new double[3];
        totalan[0] = 8.333334; //gram terigu
        totalan[1] = 6.666667; //gram gula
        totalan[2] = 6.666667; //mL susu
        int asli1 = 125, asli2=100,asli3=100;
        for (int i=0;i<totalan.length;i++){
            totalan[i] *= banyak;
        }

        if (banyak % 15 == 0) {
            int kali = banyak / 15;
            asli1 *= kali;
            asli2 *= kali;
            asli3 *= kali;
            double penampung1 = totalan[0]-asli1;
            double penampung2 = totalan[1]-asli2;
            double penampung3 = totalan[2]-asli3;
            totalan[0] -= penampung1;
            totalan[1] -= penampung2;
            totalan[2] -= penampung3;
        }
        return totalan;

    }

}
