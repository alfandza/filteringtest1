/* 
Setiap 5 stik es loli dapat ditukar dengan 1 buah es loli gratis. 
Bambang membeli beberapa es loli, memakannya, kemudian mengumpulkan stiknya 
untuk ditukar dengan es loli gratis. Jika harga es loli adalah Rp 1000, 
berapakah jumlah maksimal es loli yang bisa didapatkan Bambang dengan uang Rp xxx ?
*/


package com.xa.FilteringTest1;

import java.util.Scanner;

public class soalUjian10 {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        soalSepuluh(input);
    }

    public static void soalSepuluh(Scanner input) {
        System.out.print("Masukkan uang untuk membeli es loli : ");
        int beliEsLoli = input.nextInt();
        if (beliEsLoli < 1000){
            System.out.println("Uangnya kurang!");
            return;
        }
        System.out.println("Jumlah es Loli yang bisa didapat adalah " + jumlahEsLoli(beliEsLoli) + " buah");
    }

    public static int jumlahEsLoli(int beli){
        int hargaEsLoli = 1000;
        int count = 0;
        while (beli >= hargaEsLoli){
            count++;
            if (count % 5==0) count++;
            beli -= hargaEsLoli;
        }
        return count;

    }

}
