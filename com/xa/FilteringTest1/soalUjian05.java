/* 
Input :    1 2 1 3 4 7 1 1 5 6 1 8
Output :    1 1 1 1 1 2 3 4 5 6 7 8
*selesaikan dengan TIDAK menggunakan fungsi Sort
*/

package com.xa.FilteringTest1;

import java.util.*;

public class soalUjian05 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalLima(scan);
    }

    public static void soalLima(Scanner scan){
        System.out.print("Masukkan input : ");
        String[] masukan = scan.nextLine().split(" ");
        String[] keluar = bubbleSort(masukan);
        System.out.print("\nOutput : ");
        for (int i=0;i<keluar.length;i++){
            System.out.print(keluar[i] + " ");
        }
        System.out.println();

    }

    public static String[] bubbleSort(String[] input){
        int ganti = 0;
        for (int i=0; i< input.length; i++) {
            for (int j = 0; j < input.length; j++) {
                int a = Integer.parseInt(input[i]);
                int b = Integer.parseInt(input[j]);

                if (a < b) {
                    ganti = a;
                    input[i] = input[j];
                    input[j] = String.valueOf(ganti);
                }

            }

        }
        return input;
    }



}
