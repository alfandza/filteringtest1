/* 
Tentukan mean, median, dan modus dari deret berikut. Jika ada lebih dari 2 modus, ambil angka yang nilainya paling kecil
Contoh Input : 8 7 0 2 7 1 7 6 3 0 7 1 3 4 6 1 6 4 3
*/

package com.xa.FilteringTest1;

import java.util.Arrays;
import java.util.Scanner;

public class soalUjian03 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalTiga(scan);
    }

    public static void soalTiga(Scanner scan) {
        System.out.print("Masukkan input : ");
        String[] input = scan.nextLine().split(" ");
        int[] inputArr = new int[input.length];
        for (int i=0; i<input.length;i++){
            int temp = Integer.parseInt(input[i]);
            inputArr[i] = temp;
        }
        System.out.println("Mean : " + nilaiMean(inputArr));
        System.out.println("Median : " + nilaiMedian(inputArr));
        System.out.println(Arrays.toString(inputArr));
        System.out.println("Modus : " + nilaiModus(inputArr));
    }

    public static double nilaiMean(int[] input){
        double temp = 0;
        for (int i=0;i<input.length;i++){
            temp += input[i];
        }
        temp = temp/input.length;
        return temp;
    }

    public static int nilaiMedian(int[] input){
        Arrays.sort(input);
        if (input.length % 2 == 0) {
            int nilaiMedian1 = input.length/2;
            int nilaiMedian2 = nilaiMedian1-1;
            int temp1 = input[nilaiMedian1];
            int temp2 = input[nilaiMedian2];
            int temp = (temp1+temp2)/2;
            return temp;
        } else {
            int nilaiMedian = input.length/2;
            int temp = input[nilaiMedian];
            return temp;
        }
    }

    public static int nilaiModus(int[] input){
        int modus = input[0];
        int countA = 0, countB = 0;
        for (int i=0;i<input.length;i++){
                int tempA = input[i];
            for (int j=1;j<input.length;j++){
                int tempB = input[j];
            }
        }
        return modus;
    }
}
