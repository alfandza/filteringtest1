/* 
Tentukan nilai minimal dan maksimal dari penjumlahan 4 komponen deret ini
Contoh Input : 1 2 4 7 8 6 9
*/

package com.xa.FilteringTest1;

import java.util.Arrays;
import java.util.Scanner;

public class soalUjian04 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalEmpat(scan);
    }

    public static void soalEmpat(Scanner scan) {
        System.out.print("Masukkan input deret : ");
        String[] input = scan.nextLine().split(" ");
        System.out.print("\nMasukkan input yang mau dijumlahkan : ");
        int angkaJumlah = scan.nextInt();
        int[] inputArr = new int[input.length];
        for (int i=0; i<input.length;i++){
            int temp = Integer.parseInt(input[i]);
            inputArr[i] = temp;
        }
        Arrays.sort(inputArr);
        System.out.println("Barisnya adalah : "+Arrays.toString(inputArr));
        System.out.println("Jumlah minimal baris depan adalah : "+jumlahMinimal(inputArr,angkaJumlah));
        System.out.println("Jumlah maksimal baris belakang adalah : "+jumlahMaksimal(inputArr,angkaJumlah));
    }

    //method

    public static int jumlahMinimal(int[] input, int jumlah) {
        int temp = 0;
        for (int i=0;i<jumlah;i++){
            temp += input[i];
        }
        return temp;
    }

    public static int jumlahMaksimal(int[] input, int jumlah) {
        int temp = 0;
        for (int i=(input.length-1);i>=(input.length-jumlah);i--){
            temp += input[i];
        }
        return temp;
    }

}
