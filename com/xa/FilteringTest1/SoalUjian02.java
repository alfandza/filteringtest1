/* 
Rotasi deret: Jika diberikan sebuah deret, tentukan pola deret terakhir setelah dirotasi sebanyak n kali
Input :    Terdiri dari 1 List/array bilangan bulat dan 1 integer banyaknya rotasi
Output : Hasil rotasi

Contoh : 7 3 9 9 2 dirotasi sebanyak 3x
rotasi pertama : 3 9 9 2 7
rotasi kedua : 9 9 2 7 3
rotasi ketiga :    9 2 7 3 9

Constrains : Deret dirotasi sebanyak n kali dari depan ke belakang

*/


package com.xa.FilteringTest1;

import java.util.Arrays;
import java.util.Scanner;

public class SoalUjian02 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        soalDua(scan);
    }

    public static void soalDua(Scanner scan) {
        System.out.print("Masukkan input : ");
        String[] input = scan.nextLine().split(" ");
        System.out.print("Masukkan pergeseran : ");
        int geser = scan.nextInt();

        geserArray(input,geser);

        
    }
    
    //method
    public static void geserArray(String[] input, int geser){
        int[] output = new int[input.length];
        
        for (int i=0;i<output.length;i++){
            output[i] = Integer.parseInt(input[i]);
        }
        
        for (int h=0; h<geser;h++){
            int swap = output[0];
            for (int i=1; i<=output.length;i++){
                if (i == output.length){
                    i = 0;
                    int temp = swap;
                    output[output.length-1] = temp;
                    break;
                }
                int temp = output[i];
                output[i-1] = temp;
            }
        }
        System.out.println(Arrays.toString(output));
    }

}
